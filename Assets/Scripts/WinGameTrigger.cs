﻿using System;
using UnityEngine;

namespace Skills
{
    public class WinGameTrigger : MonoBehaviour
    {
        [SerializeField] private GameManager _gameManager;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _gameManager.WinGame();
            }
        }
    }
}