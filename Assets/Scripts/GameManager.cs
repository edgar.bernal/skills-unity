﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Skills
{
    public class GameManager : MonoBehaviour
    {

        [SerializeField] private RestartGame _restartGame;
        [SerializeField] private RestartGame _winGame;
        
        private Coroutine _endGameCoroutine;
        private Coroutine _winGameCoroutine;

        private bool _gameBlocked = false;

        public void EndGame()
        {
            if (!_gameBlocked)
            {
                _endGameCoroutine = StartCoroutine(EndGameWorker());
                _gameBlocked = true;
            }
        }

        private IEnumerator EndGameWorker()
        {
            yield return new WaitForSeconds(1.0f);
            
            _restartGame.Show();
        }
        
        public void WinGame()
        {
            if (!_gameBlocked)
            {
                _winGameCoroutine = StartCoroutine(WinGameWorker());
                _gameBlocked = true;
            }
        }

        private IEnumerator WinGameWorker()
        {
            yield return new WaitForSeconds(1.0f);
            
            _winGame.Show();
        }
    }
}