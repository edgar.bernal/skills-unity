﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skills
{
    public class FollowPlayer : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private Vector3 _offset = new Vector3(0, 5.0f, 5.0f);

        void Update()
        {
            transform.position = _target.position + _offset;
        }
    }
}