﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Skills
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private float _forwardForce = 100.0f;
        [SerializeField] private float _sideForce = 50.0f;

        [SerializeField] private Transform _cameraTransform;
        [SerializeField] private bool _useNewInputSystem = true;

        private InputMaster _inputMaster;
        private InputAction _movement;
        
        private void Awake()
        {
            _inputMaster = new InputMaster();
        }

        private void OnEnable()
        {
            _movement = _inputMaster.Player.Movement;

            //_movement.performed += MovePlayerInput;
            _movement.Enable();
        }
        
        private void OnDisable()
        {
            _movement.Disable();
        }

        /*public void MovePlayerInput(InputAction.CallbackContext context)
        {
            if (_useNewInputSystem)
            {
                MovePlayer(context.ReadValue<Vector2>().x);
            }
        }*/
        
        private void MovePlayer(float direction)
        {
            Debug.LogFormat("direction {0}", direction);
            var right = _cameraTransform.TransformDirection(Vector3.right);
            _rigidbody.AddForce(right * (direction * _sideForce));
        }

        void FixedUpdate()
        {
            var forward = _cameraTransform.TransformDirection(Vector3.forward);
            _rigidbody.AddForce(forward * _forwardForce);

            if (_useNewInputSystem)
            {
                MovePlayer(_movement.ReadValue<Vector2>().x);
            }
            else
            {
                var horizontal = Input.GetAxis("Horizontal");
                var right = _cameraTransform.TransformDirection(Vector3.right);
                _rigidbody.AddForce(right * (horizontal * _sideForce));
            }
        }
    }
}