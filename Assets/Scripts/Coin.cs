﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skills
{
    public class Coin : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                Destroy(gameObject);
            }
        }
    }
}
