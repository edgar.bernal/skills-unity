﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Skills
{
    public class RestartGame  : MonoBehaviour
    {
        private readonly int ShowTriggerHash = Animator.StringToHash("show");
        private readonly int HideTriggerHash = Animator.StringToHash("hide");
        
        [SerializeField] private Button _button;
        [SerializeField] private Animator _animator;

        private void Start()
        {
            _button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            _animator.SetTrigger(HideTriggerHash);
        }

        [UsedImplicitly]
        public void OnHideAnimationEnded()
        {
            gameObject.SetActive(false);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void Show()
        {
            gameObject.SetActive(true);
            _animator.SetTrigger(ShowTriggerHash);
        }
    }
}