﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skills
{
    public class PlayerCollision : MonoBehaviour
    {
        [SerializeField] private PlayerMovement _playerMovement;
        [SerializeField] private GameManager _gameManager;

        private void OnCollisionEnter(Collision collisionInfo)
        {
            if (collisionInfo.collider.CompareTag("Obstacle"))
            {
                _playerMovement.enabled = false;
                _gameManager.EndGame();
            }
        }
    }
}