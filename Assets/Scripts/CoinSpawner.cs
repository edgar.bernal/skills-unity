﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Skills
{
    public class CoinSpawner : MonoBehaviour
    {
        [SerializeField] private List<Transform> _randomCoins;
        [SerializeField] private GameObject _coinsPrefab;

        public void Start()
        {
            if (_randomCoins.Count == 0) return;

            var randomIndex = Random.Range(0, _randomCoins.Count);

            var randomPosition = _randomCoins[randomIndex].position;
            GameObject.Instantiate(_coinsPrefab, randomPosition, Quaternion.identity);
        }
    }
}